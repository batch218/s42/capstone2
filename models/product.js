const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name:{
		type: String,
		required: [true, "name is required"]
	},
  description:{
		type: String,
		required: [true, "description is required"]
	},
  price:{
    type: Number,
    required: [true, "price is required"]
	},
  createdOn:{
    type: Date,
    default: new Date()
  },
  orders:[{
    userId:{
      type: String,
		  required: [true, "orderId is required"]
    },
     quantity:{
            type: Number,
					  required: [true, "quantity is required"]
             },
    purchasedOn:{
        type: Date,
				default: new Date()
			  }
  }]
})

module.exports = mongoose.model("Products", productSchema);