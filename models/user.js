const mongoose = require("mongoose");
const userSchema = new mongoose.Schema({
	email:{
		type: String,
		required: [true, "email is required"]
	},
  password:{
		type: String,
		required: [true, "password is required"]
	},
  isAdmin:{
		type: Boolean,
		default: false
	},
  orders : [
		{
			products: [{
				productId:{
          type: String,
          required: [true, "productId is required"]
        },
        quantity:{
          type: Number,
					required: [true, "quantity is required"]
        }
			}],
			totalAmount:{
        type: Number
			},
      purchasedOn:{
        type: Date,
				default: new Date()
			}
		} 
	]
});

module.exports = mongoose.model("User", userSchema);