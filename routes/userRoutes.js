const express = require("express");
const router = express.Router();
const auth = require("../auth.js");
const userController = require("../controllers/userController.js");

router.post("/checkEmail", (req, res) => {
	userController.checkEmailExist(req.body).then(result => res.send(result))
});
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(result => res.send(result))
})
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(result => res.send(result))
});

router.get("/:id/userDetails", (req, res) => {
	userController.retriveUser(req.params.id).then(result => res.send(result))
});

router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
		console.log(userData)
		console.log(req.headers.authorization);
	userController.getProfile({id: userData.id}).then(result => res.send(result))
});

router.get("/All", (req, res) => {
	userController.getActiveUser().then(result => res.send(result))
})

router.post("/createOrder", (req, res) => {
	console.log(auth.decode(req.headers.authorization).id)
	let data = {
		userId : auth.decode(req.headers.authorization).id,
		productId : req.body.productId,
		quantity: req.body.quantity
		}
	userController.createOrder(data).then(result => res.send(result));
});


module.exports = router;