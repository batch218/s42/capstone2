const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController.js");
const auth = require("../auth.js");

router.post("/create", auth.verify, (req,res)=>{
	const data ={
		products: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.addProduct(data).then(result=> res.send(result))
})
router.get("/active", (req, res) => {
	productController.getActiveProducts().then(result => res.send(result))
})
router.get("/:productId", (req, res) => {
	// retrieves the id from the url
	productController.getSpecificProducts(req.params.productId).then(result => res.send(result))
})
router.patch("/:productId/update", auth.verify, 
(req,res) => 
{
	console.log(req.params)
	const newData = {
		products: req.body, 
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.updateProduct(req.params.productId, newData).then(result => {
		res.send(result)
	})
})
router.patch("/:productId/archive", auth.verify, (req,res) => 
{
	const newData = {
		products: req.body, 
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.archiveProduct(req.params.productId, newData).then(result => {
		res.send(result)
	})
});

router.get("/All", (req, res) => {
	userController.getActiveProducts().then(result => res.send(result))
})


module.exports = router;