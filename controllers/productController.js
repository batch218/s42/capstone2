const mongoose = require("mongoose");
const Products = require("../models/product.js");

module.exports.addProduct = (data) => {
	console.log(data.isAdmin)

	if(data.isAdmin) {
		let newProduct = new Products({
			name: data.products.name,
			description: data.products.description,
			price: data.products.price
		});

		return newProduct.save().then((newProduct, error) => {
			if(error){
				return error
			}

			return newProduct
		})
	};
	let message = Promise.resolve('Access is only for admin')

	return message.then((value) => {
		return {value}
	})
};
module.exports.getActiveProducts = () => {
	return Products.find({isActive:true}).then(result => {
			return result;
	})
}
module.exports.getSpecificProducts = (productId) => {
return Products.findById(productId).then(result => {
return result;
})
}

module.exports.updateProduct = (productId, newData) => {
	console.log(productId)
	console.log(newData)
	if(newData.isAdmin == true){
		return Products.findByIdAndUpdate(productId , 
			{		
				name: newData.products.name,
				description: newData.products.description,
				price: newData.products.price	
			}
		)
		.then((updateProduct, error) => {
			if(error){
				return false
			}
			return true
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this');
		return message.then((value) => {return value})
	}
}

module.exports.archiveProduct = (productId,newData) => {
	if(newData.isAdmin == true){
		return Products.findByIdAndUpdate(productId , 
			{		
				name: newData.products.name,
				description: newData.products.description,
				price: newData.products.price
			}
		).then((archiveProduct, error) => {
			if(error){
				return false
			}
			return true
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this');
		return message.then((value) => {return value})
	}
}