const User = require("../models/user.js");
const Products = require("../models/product.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

module.exports.checkEmailExist = (reqBody) => {
	return User.find({email: reqBody.email }).then(result => {
		if(result.length > 0){
			return true;
		}
		else
		{
			return false;
		}
	})
};


module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	})
	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}


module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result =>{
		if(result == null){
			return false;
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)};
			}
			else{
				return "Password is incorrect";
			}
		}
	})
}
module.exports.getActiveUser = () => {
	return User.find({isAdmin:false}).then(result => {
			return result;
	})
}
module.exports.retriveUser = (userId) => {
	return User.findById(userId).then((details, err) => {
		if(err){
			console.log(err);
			return false;
		}
		else{
			details.password = " ";
			return details;
		}
	})
}

module.exports.getProfile = (userData) => {
	return User.findById(userData.id).then(result => {	
		if (result == null) {
			return false
		} else {
			result.password = "*****"
			return result
		}
	})
};




module.exports.createOrder = async (data) => {
	console.log(data)
	let isUserUpdated = await User.findById(data.userId).then(user => {
		user.orders.push({products:{productId : data.productId, quantity : data.quantity}});
		return user.save().then((user, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});
	});
	let isProductsUpdated = await Products.findById(data.productId).then(Products => {
		Products.orders.push({userId : data.userId,quantity : data.quantity });
		return Products.save().then((Products, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		});
	
	});
	if(isUserUpdated && isProductsUpdated){
		return true;
	} else {
		return false;
	};
}
