const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// routers
const userRoutes = require("./routes/userRoutes.js");
const productRoutes = require("./routes/productRoutes.js");
const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use("/users", userRoutes);
app.use("/products", productRoutes);

mongoose.connect("mongodb+srv://admin:admin@batch218-coursebooking.2jzyijg.mongodb.net/E-Commerce", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

mongoose.connection.once('open', () => console.log('Now connected to Baal-Mongo DB Atlas.'));

app.listen(process.env.PORT || 4000, () => 
	{console.log(`API is now online on port ${process.env.PORT || 4000 }`)
});


// 3000, 4000, 5000, 8000 - Port numbers for Web applications

